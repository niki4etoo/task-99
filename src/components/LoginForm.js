import React from 'react';

import Form from './form';

export default function LoginForm(){
	return (
		<>
			<Form.Card>
				<Form.Form>
					<Form.Input type="email" />
					<Form.Input type="password" />
					<Form.Button>Login</Form.Button>
				</Form.Form>
			</Form.Card>
		</>
	);
}
